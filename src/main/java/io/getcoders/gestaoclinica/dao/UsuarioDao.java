package io.getcoders.gestaoclinica.dao;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.getcoders.gestaoclinica.model.Usuario;

@Repository
public interface UsuarioDao extends JpaRepository<Usuario, UUID>{

	Usuario findByTelefone(String telefone);
	
}
