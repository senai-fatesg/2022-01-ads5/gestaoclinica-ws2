package io.getcoders.gestaoclinica.dao;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.getcoders.gestaoclinica.model.Dispositivo;

@Repository
public interface DispositivoDao extends JpaRepository<Dispositivo, UUID>{

}
