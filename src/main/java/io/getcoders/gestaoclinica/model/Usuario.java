package io.getcoders.gestaoclinica.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
public class Usuario {

	@Id
    @GeneratedValue(generator = "UUID")	
	private UUID id;

	@Column(unique = true, nullable = false)
	private String telefone;
	
	@Column(nullable = false)
	private String wsLogin;
	
	@Column(nullable = false)
	private String wsPass;
}
