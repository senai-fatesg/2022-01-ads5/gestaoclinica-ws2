package io.getcoders.gestaoclinica.model.enums;

public enum EnumStatusDispositivo {
	
	REQUISICAO,
	ATIVO;

}
