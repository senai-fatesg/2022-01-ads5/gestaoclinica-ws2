package io.getcoders.gestaoclinica.model;

import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

import io.getcoders.gestaoclinica.model.enums.EnumStatusDispositivo;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
public class Dispositivo extends BaseEntity{
	
	@Id
    @GeneratedValue(generator = "UUID")	
	private UUID id;
	
	@ManyToOne
	private Usuario usuario;
	
	private String nome;
	
	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	private Map<String, Object> deviceInfo;
	
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private EnumStatusDispositivo status = EnumStatusDispositivo.REQUISICAO;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataRequisicao = new Date();
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataAtivacao;
	
	@Column(nullable = false)
	private String codigoAtivacao = String.format("%04d", new Random().nextInt(9999));
	
	@Column(nullable = false)
	private String telefone;
	
	private String modelo;

	public Map<String, Object> getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(Map<String, Object> deviceInfo) {
		this.deviceInfo = deviceInfo;
	}
}
