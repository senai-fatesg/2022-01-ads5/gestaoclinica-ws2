package io.getcoders.gestaoclinica.rest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.franciscocalaca.http.utils.UtilHttp;

import io.getcoders.gestaoclinica.dao.DispositivoDao;
import io.getcoders.gestaoclinica.dao.UsuarioDao;
import io.getcoders.gestaoclinica.model.Dispositivo;
import io.getcoders.gestaoclinica.model.Usuario;
import io.getcoders.gestaoclinica.model.enums.EnumStatusDispositivo;

@RestController
@RequestMapping("/rest/dispositivo")
public class DispositivoRest {

	@Value("${gestaoclinica.message.user}")
	private String user;
	
	@Value("${gestaoclinica.message.pass}")
	private String pass;
	
	@Value("${gestaoclinica.message.from}")
	private String from;
	
	@Value("${gestaoclinica.ws.user}")
	private String wsLogin;

	@Value("${gestaoclinica.ws.pass}")
	private String wsPass;
	
	@Autowired
	private DispositivoDao dispositivoDao;
	
	@Autowired
	private UsuarioDao usuarioDao;

	@GetMapping("/{uuid}")
	public Dispositivo get(@PathVariable("uuid") String uuid) {
		Optional<Dispositivo> optDisp = dispositivoDao.findById(UUID.fromString(uuid));
		return optDisp.isPresent() ? optDisp.get() : null;
	}
	
	@SuppressWarnings("unchecked")
	@PostMapping
	public String reqDev(@RequestBody Map<String, Object> dados) {
		Dispositivo dispositivo = new Dispositivo();
		dispositivo.setTelefone((String) dados.get("telefone"));
		dispositivo.setNome((String) dados.get("nome"));
		dispositivo.setModelo((String) dados.get("modelo"));
		dispositivo.setDeviceInfo((Map<String, Object>) dados.get("deviceInfo"));
		dispositivoDao.save(dispositivo);
		
		Map<String, String> headers = new HashMap<String, String>();
		Map<String, String> parameters = new HashMap<String, String>();
		Map<String, Object> bodyMap = new HashMap<String, Object>();
		bodyMap.put("user", user);
		bodyMap.put("pass", pass);
		bodyMap.put("from", from);
		bodyMap.put("to", "+55" + dispositivo.getTelefone());
		bodyMap.put("message", String.format("Seu código de validação para o app GestãoClínica é %s", dispositivo.getCodigoAtivacao()));
		
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			String body = mapper.writeValueAsString(bodyMap);
			UtilHttp.sendPost("http://zap.plug.farm/sendSms", headers, parameters, "application/json", body, "utf-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dispositivo.getId().toString();
	}
	
	@GetMapping("/ativar/{uuid}/{codigoAtivacao}")
	public Map<String, Object> ativar(@PathVariable("uuid") String uuid, @PathVariable("codigoAtivacao") String codigoAtivacao){
		Optional<Dispositivo> optDisp = dispositivoDao.findById(UUID.fromString(uuid));
		Map<String, Object> resp = new HashMap<String, Object>();
		if(optDisp.isPresent()) {
			Dispositivo dispositivo = optDisp.get();
			if(dispositivo.getCodigoAtivacao().equals(codigoAtivacao)) {
				Usuario usuario = usuarioDao.findByTelefone(dispositivo.getTelefone());
				if(usuario == null) {
					usuario = new Usuario();
					usuario.setTelefone(dispositivo.getTelefone());
					usuario.setWsLogin(wsLogin);
					usuario.setWsPass(wsPass);
					usuarioDao.save(usuario);
				}
				
				resp.put("status", "success");
				resp.put("user", usuario);
				dispositivo.setDataAtivacao(new Date());
				dispositivo.setStatus(EnumStatusDispositivo.ATIVO);
				dispositivo.setUsuario(usuario);
				dispositivoDao.save(dispositivo);
				
			}else {
				resp.put("status", "error");
				resp.put("msg", "Código de Ativação incorreto");
			}
		}else {
			resp.put("status", "error");
			resp.put("msg", "Dispositivo não encontrado");
		}
		return resp;
		
	}
}
