package io.getcoders.gestaoclinica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestaoclinicaWs2Application {

	public static void main(String[] args) {
		SpringApplication.run(GestaoclinicaWs2Application.class, args);
	}

}
