FROM openjdk:8u151-jdk-alpine
RUN apk add -U tzdata
RUN cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime
RUN apk del -U tzdata


RUN mkdir /app

COPY /target/gestaoclinica-ws2.jar /app

CMD ["java","-Dspring.profiles.active=prod","-Duser.timezone=GMT-03:00", "-Dspring.config.location=classpath:/application.yml,file:/app/config/application.yml" , "-jar" ,"/app/gestaoclinica-ws2.jar"]